function problem3(arg){
    const newDate = new Date(arg)
    return newDate.getMonth()+1
}

module.exports = problem3;