function problem4(arg){
    let fullName = ''
    const nameArray = Object.values(arg)

    for (let i=0; i<nameArray.length; i++){
        let name = ''

        for(j=0; j<nameArray[i].length; j++){
            if(j===0){
                name += nameArray[i][j].toUpperCase()
            }
            else{
                name += nameArray[i][j].toLowerCase()
            }
        }

        fullName += name + ' '
    }

    return fullName;
  
}

module.exports = problem4;