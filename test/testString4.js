const problem4 = require("../string4")

const person = {"first_name": "JoHN", "middle_name": "doe", "last_name": "SMith"}

const fullName = problem4(person);
console.log(fullName)


// {"first_name": "JoHN", "last_name": "SMith"}
//  {"first_name": "JoHN", "middle_name": "doe", "last_name": "SMith"}