function problem2(arg){
    const splitArg = arg.split(".")
    const numArray = []

    for(let i=0; i<splitArg.length; i++){
        if(Number.isInteger(Number(splitArg[i]))){
            numArray.push(Number(splitArg[i]))
        }
    }

    if(numArray.length===splitArg.length){
        return numArray;
    }
    else{
        return []
    }
}

module.exports = problem2;