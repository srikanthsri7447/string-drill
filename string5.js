function problem5(arg){
    let array = arg
    let output = ''
    for(let i=0; i<array.length; i++){
        output += (array[i]) + ' '
    }
    
    if(output.length>0){
        return output
    }
    else{
        return ''
    }
}


module.exports = problem5;